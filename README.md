# Operating system
## Overview
- Distros
- Dirrectory structure
- Users/Groups/privilage escalation
- shell/profile/history/prompt/ENV
- Remote access/SSH/KEYS
- env settings/timezone/locale
- reboot/shutdown
### Usefull commands
- man
- who
- last
- date
- env
- cat
- ls
- cd
- pwd
- adduser
- addgroup
- su
- visudo
- sudo
- mc
- lsb_release
- reboot
- shutdown
### To read
- [Linux distribution timeline](https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg)
- [Linux Filesystem Tree Overview](https://help.ubuntu.com/community/LinuxFilesystemTreeOverview)
- [How To Configure SSH Key-Based Authentication on a Linux Server](https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server)
- [Run Command on SSH Login](https://www.neilgrogan.com/ssh-login-cmd/)
- [su или sudo](https://habr.com/ru/post/44783/)
- [How To Set or Change Timezone on Ubuntu 20.04](https://linuxize.com/post/how-to-set-or-change-timezone-on-ubuntu-20-04/)
- [Set System-wide Default Language Settings on Linux using localectl](https://techviewleo.com/set-system-wide-default-language-settings-using-localectl/)
### Lab
- change default shell to sh
- suffer several minutes and return to bash
- add new user
- allow new user ot use ssh keys to get remote access
- allow new user execute reboot and shutdown command
- become new user
- reboot system
- add ssh key for new user
- make system reboot on new user login via ssh key
- allow new user to become root
- disable login for new user
- delete new user
- change current prompt
- change tz to macao
- change locale to UA
- shutdown system
## Kernel
- Boot process/grub/initramfs/init|sysytemd/runlevels
- lsmod
- lspci
- lshw
- swap
- scheduler
- multicore
- sysctl
- dmesg
### Lab
- restore root access
## Process
- top
- htop
- ps
- /proc fs
- kill
- threads
- strace
- nice
- taskset
### Lab
- emulate large nubmers of defunc process
## FileSystem
- mount
- fsck
- parted/sfdisk
- raid/mdraid
- lvm
- mkfs
- ls
- sticky bit/Setuid
- cd
- rm
- chown
- chmod
- df
- du
- lsof
- find
- which
## Services/systemd
- unitfiles/drop-ins    
- service/systemctl
- journalctl/logs
- pereodic tasks
## Network Basic
- ip a/r
- tcp/udp
- ifconfig
- ethtool
- netplan/ifconfig
- dhcp
- iptables
- nat
- netstat/ss
- tcpdump
- ping
- mtr/traceroute
## Network protocols
- ntp
- ssh
- dns
- smtp
- http(s)
## Network Service
- dns/resolv
- ntp/chrony
- ssh
- mail
## Logs
- syslog
- logrotate
## Packages
- apt
- dpkg
- unattended-upgrades
## Shell scripting
- ENV
- cat
- grep
- sort
- awk
- sed
- perl
- python
# Web Application
## Languages overview
- python
- java
- go
- php
## Archetecture overview
- monolith
- Microservice
- api
## Certificates/PKI
## Frontend
- javascript
- bootstrap
- static
## Docker
- kernel namespaces
- Runing image
- Building image
- daemon
- docker compose
- registry
- layers
## Web servers
- Apache
- php-fpm
- gunicorn
- wsgi
## Balancers
- Haproxy
- Nginx
# Chapter #4 Observability
## Monitoring
- prometheus
- influx
- ampq
- grafana
## Log analyse
- ELK
- fluentd/filebeat
# Chapter #5 dbs
## Common tasks
- Replication
- Balancing
- Backup/restore    
## Reletionaldbs
- indexes
- sql    
- Postgresql
- Mysql
## Non relational dbs
- MongoDB
- ClickHouse
- Elasticsearch
- memcache
- redis
## Queues
- rabbitmq
- kafka
# Chapter #6 Orchestration
## Ansible/Salt/Puppet overview
## Ansible
- inventory
- tasks
- roles
- notifier
- templating/ jinja
- clauses
# Chapter #7 CI/CD
## Git/Gitlab
- pull
- push
- branches
- merge
## Pipelines
- stage
- build
## Tests
## Package build
## Deploy
# Chapter #8 Kubernetes
## Main patrs
- etcd
- api-server
- scheduler    
- kubelet
- kube-proxy
- core-dns
- cni/calico
## Bootstrab
- kubadm
- kubectl
## Default Resources
- pod
- deployment
- statefulset
- service
- secret
## Operators
- main concept
## istio
## nginx ingres
# Chapter #9 Clouds
## vpc
## az/regions
## vms
## policies
## balancers
## terraform
## aws
## gcp
## azure
# Profit !!!
